<?php

require ("animal.php");
require ("Frog.php");
require ("ape.php");

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "legs :$sheep->legs <br>"; // 4
echo "blooded :$sheep->cold_blooded<br>"; 

echo "<br>";// "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$kodok = new Frog("buduk");

echo "Name : $kodok->name <br>"; 
echo "legs : $kodok->legs <br>";
echo "blooded : $kodok->cold_blooded<br>";
echo "Jump : $kodok->jump <br>" ; // "hop hop"

echo "<br>";
$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name <br>";
echo "Legs  : $sungokong->legs <br>";
echo "blooded : $sungokong->cold_blooded<br>";
echo "Yell : $sungokong->yell";



